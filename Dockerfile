FROM ekidd/rust-musl-builder:nightly-2019-07-08 AS builder
ADD . ./
RUN sudo chown -R rust:rust /home/rust
RUN cargo build --release

FROM alpine:latest
RUN apk --no-cache add ca-certificates
COPY --from=builder \
    /home/rust/src/target/x86_64-unknown-linux-musl/release/foli-static-display \
    /usr/local/bin/
CMD /usr/local/bin/foli-static-display