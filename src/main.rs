#![feature(proc_macro_hygiene, decl_macro)]

use handlebars::{Context, Handlebars, Helper, Output, RenderContext, RenderError};
use chrono::prelude::*;
use serde::{Deserialize, Serialize};

use rocket::{routes, get, State};
use rocket::http::{Status, ContentType};
use rocket::response::Content;

const TIMETABLE_TEMPLATE: &str = r#"
<!DOCTYPE html>
<html>
    <head>
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="refresh" content="{{this.seconds_to_refresh}}">
        <link rel="stylesheet" type="text/css" href="foli.css">
    </head>
    <body>
        <div id="clock">{{time}}</div>
        <div id="listsWrapper">
            <div class="tripsListWrapper" id="tripsTableLeft">
                <ul>
                    {{#if this.left_has_error}}
                    <li class="trip"><img class="error_image" src="error.png" /></li>
                    {{else}}
                    {{#each left}}
                    <li class="trip">
                        <div class="tripLineWrapper">
                            <span class="shortName">{{this.trip}}</span>
                            <span class="departureTime {{comingSoon this.departure_time}}">{{#if this.realtime}}<img class="liveIcon" src="live.png">{{/if}}{{departureDate this.departure_time}}</span>
                        </div>
                        <div class="tripLineWrapper">
                            <span class="stopCode">{{this.stop}}</span>
                            <span class="longName">{{this.headsign}}</span>
                        </div>
                    </li>
                    {{/each}}
                    {{/if}}
                </ul>
            </div>
            <div class="tripsListWrapper" id="tripsTableRight">
                <ul>
                    {{#if this.right_has_error}}
                    <li class="trip"><img class="error_image" src="error.png" /></li>
                    {{else}}
                    {{#each right}}
                    <li class="trip">
                        <div class="tripLineWrapper">
                            <span class="shortName">{{this.trip}}</span>
                            <span class="departureTime {{comingSoon this.departure_time}}">{{#if this.realtime}}<img class="liveIcon" src="live.png">{{/if}}{{departureDate this.departure_time}}</span>
                        </div>
                        <div class="tripLineWrapper">
                            <span class="stopCode">{{this.stop}}</span>
                            <span class="longName">{{this.headsign}}</span>
                        </div>
                    </li>
                    {{/each}}
                    {{/if}}
                </ul>
            </div>
        </div>
    </body>
</html>
"#;

fn comingsoon_helper(
    h: &Helper,
    _: &Handlebars,
    _: &Context,
    _: &mut RenderContext,
    out: &mut dyn Output,
) -> Result<(), RenderError> {
    // get parameter from helper or throw an error
    let param = h
        .param(0)
        .and_then(|v| v.value().as_i64())
        .ok_or(RenderError::new(
            "Param with i64 type is required for comingSoon helper.",
        ))?;

    let departure_time = Local.timestamp(param, 0);
    let now = Local::now();
    let difference: chrono::Duration = departure_time - now;

    if difference.num_minutes() < 1 {
        out.write("comingSoon")?;
    }
    Ok(())
}

fn departure_date_helper(
    h: &Helper,
    _: &Handlebars,
    _: &Context,
    _: &mut RenderContext,
    out: &mut dyn Output,
) -> Result<(), RenderError> {
    // get parameter from helper or throw an error
    let param = h
        .param(0)
        .and_then(|v| v.value().as_i64())
        .ok_or(RenderError::new(
            "Param with i64 type is required for departureDate helper.",
        ))?;

    let departure_time = Local.timestamp(param, 0);
    let now = Local::now();
    let difference: chrono::Duration = departure_time - now;

    if difference.num_minutes() < 5 {
        out.write(&format!("{} min", difference.num_minutes()))?;
    } else {
        out.write(&departure_time.format("%H:%M").to_string())?;
    }
    Ok(())
}

#[derive(Deserialize)]
struct FoliSiriSmResult {
    status: String,
    result: Option<Vec<FoliSiriSmBusTrip>>,
}

#[derive(Deserialize)]
struct FoliSiriSmBusTrip {
    lineref: String,
    destinationdisplay: String,
    expecteddeparturetime: i64,
    monitored: bool,
}

#[derive(Debug)]
enum LoadError {
    ServerError(reqwest::Error),
    DataError,
    FoliError,
}

#[derive(Serialize)]
struct PageInfo {
    time: String,
    left: Vec<BusTrip>,
    right: Vec<BusTrip>,
    left_has_error: bool,
    right_has_error: bool,
    seconds_to_refresh: i64,
}

#[derive(Serialize, Debug)]
struct BusTrip {
    trip: String,
    headsign: String,
    departure_time: i64,
    realtime: bool,
    stop: String,
}

fn get_stop_list(stop: &str) -> Result<Vec<BusTrip>, LoadError> {
    Ok(
        reqwest::get(&format!("https://data.foli.fi/siri/sm/{}", stop))
            .map_err(|e| LoadError::ServerError(e))?
            .json::<FoliSiriSmResult>()
            .map_err(|_| LoadError::DataError)
            .and_then(|r| {
                if r.status == "OK" {
                    Ok(r)
                } else {
                    Err(LoadError::FoliError)
                }
            })?
            .result
            .ok_or(LoadError::DataError)?
            .drain(..)
            .map(|r| BusTrip {
                trip: r.lineref,
                headsign: r.destinationdisplay,
                departure_time: r.expecteddeparturetime,
                realtime: r.monitored,
                stop: stop.to_owned(),
            })
            .collect(),
    )

}

fn seconds_to_refresh(left: &Vec<BusTrip>, right: &Vec<BusTrip>) -> i64 {
    let now = Local::now();

    let seconds = left.iter().chain(right.iter()).fold(None, |acc, x| acc.map(|y| Ord::max(x.departure_time, y)));

    if let Some(seconds) = seconds {
        let closest_departure_time = Local.timestamp(seconds, 0);
        let difference: chrono::Duration = closest_departure_time - now;

        if difference.num_seconds() < 5 {
            2
        } else if difference.num_seconds() > 15 {
            15
        } else {
            difference.num_seconds()-2
        }
    } else {
        15
    }
}

#[get("/?<left>&<right>")]
fn handle(left: Option<String>, right: Option<String>, handlebars: State<Handlebars>) -> Result<Content<String>, Status> {
        let left_id = left.as_ref().map(String::as_str).unwrap_or(&"219");
        let right_id = right.as_ref().map(String::as_str).unwrap_or(&"280");

        let left = get_stop_list(left_id);
        let right = get_stop_list(right_id);
        let left_has_error = left.is_err();
        let right_has_error = right.is_err();

        let time: DateTime<Local> = Local::now();

        let refresh = seconds_to_refresh(left.as_ref().unwrap_or(&vec![]), right.as_ref().unwrap_or(&vec![]));

        let page_info = PageInfo {
            time: format!("{}", time.format("%H:%M")),
            left_has_error: left_has_error,
            right_has_error: right_has_error,
            left: left.unwrap_or(vec![]),
            right: right.unwrap_or(vec![]),
            seconds_to_refresh: refresh
        };

        handlebars.render("display", &page_info)
            .map(|r| Content(ContentType::HTML, r))
            .map_err(|_| Status::InternalServerError)
}

fn main() {
    let mut handlebars = Handlebars::new();
    handlebars
        .register_template_string("display", TIMETABLE_TEMPLATE)
        .unwrap();
    handlebars.register_helper("comingSoon", Box::new(comingsoon_helper));
    handlebars.register_helper("departureDate", Box::new(departure_date_helper));

    let config = rocket::Config::build(rocket::config::Environment::Production)
        .port(8088)
        .finalize().unwrap();

    rocket::custom(config)
        .manage(handlebars)
        .mount("/", routes![handle])
        .launch();
}